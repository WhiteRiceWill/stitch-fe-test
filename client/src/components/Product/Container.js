import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateProduct, deleteProduct } from '../../actions/products';
import Product from './Component';

// Pass state to component as props
const mapStateToProps = state => ({
  saveInProgess: state.productsReducer.items.saveInProgess,
});

// Pass actions to the component as props
const mapDispatchToProps = dispatch => bindActionCreators({
  updateProduct,
  deleteProduct,
}, dispatch);

// Connect to Redux
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Product);
