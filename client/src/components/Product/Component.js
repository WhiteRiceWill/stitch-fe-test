import React, { Component } from 'react';
import styles from './Product.module.css';

class Product extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      editMode: false,
      title: '',
      description: '',
      sku: '',
      inventory: '',
      price: '',
      weight: '',
    };

    this.toggleOpen = this.toggleOpen.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.saveEdit = this.saveEdit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.deleteProduct = this.deleteProduct.bind(this);
  }

  componentDidUpdate(prevProps) {
    // When saving an edit finishes exit editing mode 
    if (prevProps.saveInProgess && !this.props.saveInProgess) {
      this.setState({
        editMode: false,
      });
    }
  }

  toggleOpen(e) {
    e.preventDefault();
    this.setState({ open: !this.state.open });
  }

  toggleEditMode(e) {
    e.preventDefault();
    this.setState({
      editMode: !this.state.editMode,
      title: this.props.title,
      description: this.props.description,
      sku: this.props.sku,
      inventory: this.props.inventory,
      price: this.props.price,
      weight: this.props.weight,
    });
  }

  saveEdit(e) {
    e.preventDefault();

    this.props.updateProduct({
      id: this.props.id,
      title: this.state.title,
      description: this.state.description,
      variantId: this.props.variantId,
      sku: this.state.sku,
      inventory: this.state.inventory,
      price: this.state.price,
      weight: this.state.weight,
    })
  }

  deleteProduct(e) {
    e.preventDefault();

    this.props.deleteProduct(this.props.id)
  }

  handleChange(e, field) {
    this.setState({ [field]: e.target.value });
  }

  render() {
    if (this.state.open) {
      if (this.state.editMode) {
        // Open editing product
        return (
          <div className={styles.boxOpen}>

            <div className={styles.topBox}>
              <div className={styles.title}>
                {this.props.title}
              </div>
              <div className={styles.inventory}>
                {this.props.inventory} in stock
              </div>
            </div>

            <div className={styles.head}>
              Product Title:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.title} onChange={(e) => { this.handleChange(e, 'title') }} />

            <div className={styles.head}>
              Inventory:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.inventory} onChange={(e) => { this.handleChange(e, 'inventory') }} />

            <div className={styles.head}>
              SKU (Stock Keeping Unit):
            </div>
            <input className={styles.inputBox} type="text" value={this.state.sku} onChange={(e) => { this.handleChange(e, 'sku') }} />

            <div className={styles.head}>
              Price:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.price} onChange={(e) => { this.handleChange(e, 'price') }} />

            <div className={styles.head}>
              Weight:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.weight} onChange={(e) => { this.handleChange(e, 'weight') }} />

            <div className={styles.head}>
              Description:
            </div>
            <textarea className={styles.descriptionInputBox} type="text" value={this.state.description} onChange={(e) => { this.handleChange(e, 'description') }} />

            <div className={styles.buttons}>
              <div className={styles.cancelButton} onClick={this.toggleEditMode}>
                Cancel
              </div>
              <div className={styles.editButtonOpen} onClick={this.saveEdit}>
                {this.props.saveInProgess ?
                  <div className={styles.loader}></div> :
                  'Save'
                }
              </div>
            </div>
          </div>
        );
      } else {
        // Open non-editing product
        return (
          <div className={styles.boxOpen}>
            <div className={styles.topBox}>
              <div className={styles.title}>
                {this.props.title}
              </div>
              <div className={styles.inventory}>
                {this.props.inventory} in stock
              </div>
            </div>

            <div className={styles.head}>
              SKU (Stock Keeping Unit):
            </div>
            <div className={styles.data}>
              {!this.props.sku ? 'No SKU assigned' : this.props.sku}
            </div>

            <div className={styles.head}>
              Price:
            </div>
            <div className={styles.data}>
              ${this.props.price}
            </div>

            <div className={styles.head}>
              Weight:
            </div>
            <div className={styles.data}>
              {this.props.weight} {this.props.weight === 1 ? 'lb' : 'lbs'}
            </div>

            <div className={styles.head}>
              Description:
            </div>
            <div className={styles.data}>
              {!this.props.description ? 'No description' : this.props.description}
            </div>
            
            <div className={styles.buttons}>
              <div className={this.props.saveInProgess ? styles.deleteButtonLoading : styles.deleteButton} onClick={this.deleteProduct}>
                {this.props.saveInProgess ?
                  <div className={styles.loader}></div> :
                  'Delete Product'
                }
              </div>
              <div className={styles.editButtonOpen} onClick={this.toggleEditMode}>
                Edit Product
              </div>
            </div>

            <img className={styles.upArrow} onClick={this.toggleOpen} src={require('../../assets/downArrow.svg')} />
          </div>
        );        
      }
    } else {
      // Closed Product
      return (
        <div className={styles.boxClosed}>
          <div className={styles.title}>       
            {this.props.title}
          </div>
          <div className={styles.inventory}>
            {this.props.inventory} in stock
          </div>
          <img className={styles.downArrow} onClick={this.toggleOpen} src={require('../../assets/downArrow.svg')} />
        </div>
      );
    }
  }
}

export default Product;
