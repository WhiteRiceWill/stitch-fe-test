import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addProduct } from '../../actions/products';
import AddProduct from './Component';

// Pass state to component as props
const mapStateToProps = state => ({
  saveInProgess: state.productsReducer.items.saveInProgess,
});

// Pass actions to the component as props
const mapDispatchToProps = dispatch => bindActionCreators({
  addProduct,
}, dispatch);

// Connect to Redux
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddProduct);
