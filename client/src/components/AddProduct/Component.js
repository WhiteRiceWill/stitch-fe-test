import React, { Component } from 'react';
import styles from './AddProduct.module.css';

class AddProduct extends Component {

  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      title: '',
      description: '',
      sku: '',
      inventory: '',
      price: '',
      weight: '',
    };

    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addProduct = this.addProduct.bind(this);
  }

  componentDidUpdate(prevProps) {
    // When product has been added set close the box
    if (prevProps.saveInProgess && !this.props.saveInProgess) {
      this.setState({
        editMode: false,
      });
    }
  }

  toggleEditMode(e) {
    e.preventDefault();
    this.setState({
      editMode: !this.state.editMode,
      title: '',
      description: '',
      sku: '',
      inventory: '',
      price: '',
      weight: '',
    });
  }

  handleChange(e, field) {
    this.setState({ [field]: e.target.value });
  }

  addProduct(e) {
    e.preventDefault();

    this.props.addProduct({
      title: this.state.title,
      description: this.state.description,
      sku: this.state.sku,
      inventory: this.state.inventory,
      price: this.state.price,
      weight: this.state.weight,
    })
  }
  
  render() {
    if (this.state.editMode) {
      return (
        <div>
          <div className={styles.boxOpen}>

            <div className={styles.topBox}>
              <div className={styles.title}>
                + Add a new product
              </div>
            </div>

            <div className={styles.head}>
              Product Title:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.title} onChange={(e) => { this.handleChange(e, 'title') }} />

            <div className={styles.head}>
              Inventory:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.inventory} onChange={(e) => { this.handleChange(e, 'inventory') }} />

            <div className={styles.head}>
              SKU (Stock Keeping Unit):
            </div>
            <input className={styles.inputBox} type="text" value={this.state.sku} onChange={(e) => { this.handleChange(e, 'sku') }} />

            <div className={styles.head}>
              Price:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.price} onChange={(e) => { this.handleChange(e, 'price') }} />

            <div className={styles.head}>
              Weight:
            </div>
            <input className={styles.inputBox} type="text" value={this.state.weight} onChange={(e) => { this.handleChange(e, 'weight') }} />

            <div className={styles.head}>
              Description:
            </div>
            <textarea className={styles.descriptionInputBox} type="text" value={this.state.description} onChange={(e) => { this.handleChange(e, 'description') }} />

            <div className={styles.buttons}>
              <div className={styles.cancelButton} onClick={this.toggleEditMode}>
                Cancel
              </div>
              <div className={styles.editButtonOpen} onClick={this.addProduct}>
                {this.props.saveInProgess ?
                  <div className={styles.loader}></div> :
                  'Add Product'
                }
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <img className={styles.addButton} onClick={this.toggleEditMode} src={require('../../assets/addIcon.svg')} />
        </div>
      );
    }
  }
}

export default AddProduct;
