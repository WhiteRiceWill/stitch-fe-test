import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import App from './Component';

// Pass state to component as props
const mapStateToProps = state => ({
  sampleItem: state.productsReducer.items.products,
});

// Pass actions to the component as props
const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);

// Connect to Redux
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
