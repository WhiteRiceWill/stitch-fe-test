import React, { Component } from 'react';
import styles from './Products.module.css';
import Product from '../Product/Container';

class Products extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const products = this.props.products;
    let productsList = null;

    // Create list of Product components and give them correct data mapped from products
    if (products !== 'loading') {
      productsList = Object.keys(products).map(function (key) {

        // Strip the description text out of the html that is provided
        let html = products[key].body_html;
        let div = document.createElement('div');
        div.innerHTML = html;
        let text = div.textContent || div.innerText || '';

        return <Product
          key={products[key].id}
          addMode={false}
          id={products[key].id}
          title={products[key].title}
          description={text}
          createdAt={products[key].created_at}
          variantId={products[key].variants[0].id}
          sku={products[key].variants[0].sku}
          inventory={products[key].variants[0].inventory_quantity}
          price={products[key].variants[0].price}
          weight={products[key].variants[0].weight}
        />
      });
    }

    return (
      <div className={styles.title}>
        {products !== 'loading' ? productsList : null}
      </div>
    );
  }
}

export default Products;
