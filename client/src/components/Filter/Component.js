import React, { Component } from 'react';
import styles from './Filter.module.css';

class Filter extends Component {

  constructor(props) {
    super(props);

    this.state = {
      titleFilter: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    // Gets all products when user makes filter empty again
    if (prevState.titleFilter.length > 0 && this.state.titleFilter === '') {
      this.props.getProducts(this.state.titleFilter);
    }
  }

  handleChange(e) {
    this.setState({ titleFilter: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.getProducts(this.state.titleFilter);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          {
          //<img className={styles.magnify} onClick={this.toggleOpen} src={require('../../assets/magnify.png')} />
          }

          <input
            className={styles.filterBox}
            placeholder="Search products"
            type="text"
            value={this.state.titleFilter}
            onChange={this.handleChange}
          />
          { this.props.products === 'loading' &&
            <div className={styles.loader}></div>
          }
        </form>
      </div>
    );
  }
}

export default Filter;
