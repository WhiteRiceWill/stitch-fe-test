import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProducts } from '../../actions/products';
import Filter from './Component';

// Pass state to component as props
const mapStateToProps = state => ({
  products: state.productsReducer.items.products,
});

// Pass actions to the component as props
const mapDispatchToProps = dispatch => bindActionCreators({
  getProducts,
}, dispatch);

// Connect to Redux
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Filter);
