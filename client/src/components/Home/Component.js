import React, { Component } from 'react';
import styles from './Home.module.css';
import Products from '../Products/Container';
import Filter from '../Filter/Container';
import AddProduct from '../AddProduct/Container';

class Home extends Component {

  componentDidMount() {
    this.props.getProducts();
  }
  
  render() {

    return (
      <div className={styles.page}>
        <div className={styles.title}>
          🏄‍♀️ Sally's Surf Shop
        </div>
        
        <Filter />
        <Products />
        <AddProduct />

        <div className={styles.buffer}>
        </div>
      </div>
    );
  }
}

export default Home;
