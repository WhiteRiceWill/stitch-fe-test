import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProducts } from '../../actions/products';
import Home from './Component';

// Pass state to component as props
const mapStateToProps = state => ({
});

// Pass actions to the component as props
const mapDispatchToProps = dispatch => bindActionCreators({
  getProducts,
}, dispatch);

// Connect to Redux
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
