import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import productsReducer from './productsReducer';

// Combine the reducers and routing middleware
const rootReducer = combineReducers({
  routing: routerReducer,
  productsReducer,
});

export default rootReducer;
