const initialState = {
  items: {
    products: 'loading',
    saveInProgess: false,
  },
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    // Update an item
    case 'UPDATE_ITEM':
      return (() => {
        const newState = Object.assign({}, state);
        newState.items[action.itemName] = action.itemValue;
        return newState;
      })();

    default:
      return state;
  }
};

export default productsReducer;
