export const updateItem = (itemName, itemValue) => ({
  type: 'UPDATE_ITEM',
  itemName,
  itemValue,
});
