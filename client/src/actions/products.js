import { updateItem } from './item.js';

export const getProducts = (titleFilter) => {
  return async (dispatch) => {
    try {
      dispatch(updateItem('products', 'loading'));

      // Filtered products url
      let reqUrl = `http://localhost:3000/shopify?path=products.json?title=${titleFilter}`;

      // All products url
      if (!titleFilter) {
        reqUrl = `http://localhost:3000/shopify?path=products.json`;
      }

      const response = await fetch(reqUrl);
      if (!response.ok) {
        throw new Error();
      }
      const data = await response.json();

      // Convert products arr to obj
      const productsObj = {};
      for (let product of data.products) {
        productsObj[product.id] = product;
      }
      dispatch(updateItem('products', productsObj));
    }
    catch (err) {
      console.log('Could not load products');
    }
  };
};


export const updateProduct = (productData) => {
  return async (dispatch, getState) => {
    try {
      dispatch(updateItem('saveInProgess', true));

      const productDataObj = {
        product: {
          id: productData.id,
          title: productData.title,
          body_html: productData.description,
          variants: [
            {
              id: productData.variantId,
              sku: productData.sku,
              inventory_quantity: productData.inventory,
              price: productData.price,
              weight: productData.weight,
            },
          ],
        },
      };

      const requestOptions = {
        method: 'PUT',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(productDataObj)
      };

      const response = await fetch(`http://localhost:3000/shopify?path=products/${productData.id}.json`, requestOptions);
      if (!response.ok) {
        throw new Error();
      }
      const data = await response.json();
      
      // Clone object to avoid reference issues
      const productsObj = JSON.parse(JSON.stringify(getState().productsReducer.items.products));

      productsObj[data.product.id] = data.product;

      dispatch(updateItem('products', productsObj));
      dispatch(updateItem('saveInProgess', false));
    }
    catch (err) {
      dispatch(updateItem('saveInProgess', false));
    }
  };
};


export const addProduct = (productData) => {
  return async (dispatch, getState) => {
    try {
      dispatch(updateItem('saveInProgess', true));

      const productDataObj = {
        product: {
          title: productData.title,
          body_html: productData.description,
          variants: [
            {
              sku: productData.sku,
              inventory_quantity: productData.inventory,
              price: productData.price,
              weight: productData.weight,
            },
          ],
        },
      };

      const requestOptions = {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(productDataObj)
      };

      const response = await fetch('http://localhost:3000/shopify?path=products.json', requestOptions);
      if (!response.ok) {
        throw new Error();
      }
      const data = await response.json();
      // Clone object to avoid reference issues
      const productsObj = JSON.parse(JSON.stringify(getState().productsReducer.items.products));

      productsObj[data.product.id] = data.product;

      dispatch(updateItem('products', productsObj));
      dispatch(updateItem('saveInProgess', false));
    }
    catch (err) {
      dispatch(updateItem('saveInProgess', false));
    }
  };
};

export const deleteProduct = (productId) => {
  return async (dispatch, getState) => {
    try {
      dispatch(updateItem('saveInProgess', true));

      const requestOptions = {
        method: 'DELETE',
        headers: { 'content-type': 'application/json' },
      };

      const response = await fetch(`http://localhost:3000/shopify?path=products/${productId}.json`, requestOptions)

      if (!response.ok) { 
        throw new Error();
      }

      // Clone object to avoid reference issues
      const productsObj = JSON.parse(JSON.stringify(getState().productsReducer.items.products));

      delete productsObj[productId];

      dispatch(updateItem('products', productsObj));
      dispatch(updateItem('saveInProgess', false));
    }
    catch (err) {
      dispatch(updateItem('saveInProgess', false));
    }
  };
};
