# Shortcomings

- Unique SKUs are not enforced. I assumed there is a way to handle this via Shopify configuration, as this isn't something that is ideal for frontend validation
- The current implementation doesn't account for many of the possible edge cases for input and does not validate/sanitize input on the client first
- Client implementation doesn't display API errors to the user, such as adding a new product with no title will result in no product being created, but no error message
- There is an interesting bug where if I create a new product through my custom client, then on Shopify's client it reads "Unavailable on Sally's Surf Shop Inventory Management App". The product still shows up on my custom client and works as expected except there is one variable I can't change on it which is inventory quantity. However, I can change the inventory quantity through my custom app if the product was created via Shopify's app. Admittedly I didn't spend too much time on this bug and moved on, but it definitely needs some extra attention.
- If I were to spend more time on this I would definitely break out some parts of the product component into more component, as there is some repetetive code in there.
- After analysis of scope/timeframe, I opted to exclude the complexity of multiple variants
- Due to scope/timeframe, the code hasn't been linted and needs more commenting/documentation
- Due to scope/timeframe, I didn't get to the optional features

# Thoughts

- Architecture and separation of responsibility are achieved through React's modular nature of using components and Redux's state management flow. I used stateful presentational components to handle UI state and container components that synced up with Redux to manage product data. I used redux-thunk to handle the asynchronous dispatching of actions when I made API calls to Shopify
- Instructions were straight forward and Shopify's API was well documented and easy to work with (especially because the prebuilt express server was already adding authentication to the calls)
- In an effort to be efficient with API calls I used Shopify's API response to update the client's product data rather than making an extra GET request. The tradeoff here is that state isn't going to be synced perfectly, for example, if I updated a product on my custom client and someone else updated another product via Shopify's client, then I would only be getting the change that I had just made, not the other user's change. I can see how a user's expectation may be that the client syncs up 100% every time you make a change, so looking back perhaps it would make sense to just make the extra call as the UI/UX benefit would outweigh the bandwidth/compute cost.